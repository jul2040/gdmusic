extends Node

export(int) var channel = 2
export(int) var velocity = 0xFF
export(int) var instrument = 0
export(int) var note_offset = 0

func _ready():
	MidiServer.set_instrument(channel, instrument)

func set_instrument(i:int):
	MidiServer.set_instrument(channel, i)

var chords = []
func set_chords(c:Array):
	chords = c

var cur_chord:int = 0
var cur_note:int = 0
func play_next():
	var chord = chords[cur_chord]
	MidiServer.note_off(channel, chord[cur_note]+note_offset)
	cur_note = wrapi(cur_note+1, 0, len(chord))
	if(cur_note == 0):
		cur_chord = wrapi(cur_chord+1, 0, len(chords))
		chord = chords[cur_chord]
	MidiServer.note_on(channel, chord[cur_note]+note_offset, velocity)

func stop():
	if(cur_chord >= len(chords)):
		return
	var chord = chords[cur_chord]
	if(cur_note >= len(chord)):
		return
	MidiServer.note_off(channel, chord[cur_note]+note_offset)

func reset():
	cur_chord = 0
	cur_note = 0
