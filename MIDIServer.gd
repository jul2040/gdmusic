extends Node

func note_on(channel:int, note:int, velocity:int=0xFF):
	var input_event:InputEventMIDI = InputEventMIDI.new()
	input_event.message = 0x09 # note on
	input_event.pitch = note # note number
	input_event.velocity = velocity # velocity
	input_event.channel = channel
	$MIDIPlayer.receive_raw_midi_message(input_event)

func note_off(channel:int, note:int):
	var input_event:InputEventMIDI = InputEventMIDI.new()
	input_event.message = 0x08 # note off
	input_event.pitch = note # note number
	input_event.channel = channel
	$MIDIPlayer.receive_raw_midi_message(input_event)

func set_instrument(channel:int, instrument:int):
	var input_event:InputEventMIDI = InputEventMIDI.new()
	input_event.message = 0x0C # change instrument
	input_event.instrument = instrument
	input_event.channel = channel
	$MIDIPlayer.receive_raw_midi_message(input_event)

func set_volume(v:float):
	$MIDIPlayer.volume_db = linear2db(v)
