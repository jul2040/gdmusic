extends Node

var default_intervals = [0,1,1,2,2,3,3,3,8,8,5,4,6]
var intervals = default_intervals
export(int) var channel = 0
export(int) var velocity = 0xFF
export(int) var velocity_range = 10
export(int) var instrument = 3
export(int) var max_note = 90
export(int) var min_note = 60
export(float) var chordiness = 0.5

func _ready():
	MidiServer.set_instrument(channel, instrument)

func set_instrument(i:int):
	MidiServer.set_instrument(channel, i)

func random_interval():
	return [1,-1][randi()%2]*intervals[randi()%len(intervals)]

var melody:Array = []
func generate_melody(scale:Array, chords:Array, length:int):
	melody = []
	melody.append(randi()%len(scale))
	for i in range(1, length+1, 1):
		if(randf() > chordiness):
			melody.append(melody[i-1]+random_interval())
		else:
			var chord = chords[int((float(i)/float(length))*(len(chords)-1))]
			melody.append(chord[randi()%len(chord)])
	for i in range(len(melody)):
		melody[i] = scale_deg(scale, melody[i])

func scale_deg(scale, d):
	var d_wrapped = wrapi(d, 0, len(scale))
	var out = scale[d_wrapped]+((d-d_wrapped)/len(scale))*12
	while(out < min_note):
		out += 12
	while(out > max_note):
		out -= 12
	return out

var cur_note:int = 0
func play_next(play:bool=true):
	#turn the previous note off
	MidiServer.note_off(channel, melody[cur_note])
	#turn next note on
	cur_note = wrapi(cur_note+1, 0, len(melody)-1)
	if(play):
		MidiServer.note_on(channel, melody[cur_note], velocity+((randi()%velocity_range)+velocity_range/2))

func stop():
	if(cur_note >= len(melody)):
		return
	MidiServer.note_off(channel, melody[cur_note])

func reset():
	cur_note = 0
