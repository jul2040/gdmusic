extends Node

export(int,"Ionian","Dorian","Phyrigian","Lydian","Mixolydian","Aeolian","Locrian") var mode:int = 0
export(int) var key:int = 40
export(int) var num_chords:int = 4
export(int) var num_melody:int = 16
export(int) var bars_to_mutate:int = 8
export(int) var beats_to_bar:int = 4
export(float) var arp_subdiv:float = 2
export(bool) var melody:bool = true
export(bool) var chords:bool = true
export(bool) var arp:bool = false
export(bool) var drums:bool = false
export(int) var bpm:float = 100

const INTERVALS = [2, 2+2, 1+2+2, 2+1+2+2, 2+2+1+2+2, 2+2+2+1+2+2, 1+2+2+2+1+2+2]

var scale
export(Array, bool) var pattern:Array
func _ready():
	randomize()

func load_song(song:Dictionary):
	key = song["key"]
	mode = song["mode"]
	bpm = song["bpm"]
	beats_to_bar = song["beats_to_bar"]
	chords = song["chords"]
	arp = song["arp"]
	melody = song["melody"]
	bars_to_mutate = song["bars_to_mutate"]
	if(arp or chords):
		num_chords = song["num_chords"]
		$ChordPlayer.base_tones = song["chord_base_tones"]
		$ChordPlayer.extra_tones = song["chord_extra_tones"]
	
	if(chords):
		$ChordPlayer.set_instrument(song["chord_instrument"])
		$ChordPlayer.velocity = song["chord_velocity"]
		$ChordPlayer.note_offset = song["chord_note_offset"]
	
	if(arp):
		arp_subdiv = song["arp_subdiv"]
		$ArpPlayer.set_instrument(song["arp_instrument"])
		$ArpPlayer.velocity = song["arp_velocity"]
		$ArpPlayer.note_offset = song["arp_note_offset"]
	
	if(melody):
		num_melody = song["num_melody"]
		pattern = song["pattern"].duplicate()
		pattern_mutate_amount = song["pattern_mutate_amount"]
		$MelodyPlayer.chordiness = song["chordiness"]
		$MelodyPlayer.set_instrument(song["melody_instrument"])
		$MelodyPlayer.velocity = song["melody_velocity"]
		$MelodyPlayer.velocity_range = song["melody_velocity_range"]
		$MelodyPlayer.max_note = song["melody_max_note"]
		$MelodyPlayer.min_note = song["melody_min_note"]
		if(song.has("interval_override")):
			$MelodyPlayer.intervals = song["interval_override"].duplicate()
		else:
			$MelodyPlayer.intervals = $MelodyPlayer.default_intervals
	
	beat = beats_to_bar
	mutate_timer = 0
	scale = []
	for i in range(len(INTERVALS)):
		scale.append(key+INTERVALS[wrapi(i+mode, 0, len(INTERVALS))])

func play():
	$ChordPlayer.generate_chords(scale, num_chords)
	$ArpPlayer.set_chords($ChordPlayer.chords)
	$MelodyPlayer.generate_melody(scale, $ChordPlayer.chords, num_melody)
	$BeatTimer.wait_time = 60.0/bpm
	_on_BeatTimer_timeout()
	$BeatTimer.start()
	$ArpTimer.wait_time = $BeatTimer.wait_time/arp_subdiv
	_on_ArpTimer_timeout()
	$ArpTimer.start()
	$DrumPlayer.beats_to_bar = beats_to_bar

func stop():
	$ChordPlayer.stop()
	$MelodyPlayer.stop()
	$ArpPlayer.stop()
	$BeatTimer.stop()
	$ArpTimer.stop()

var pattern_mutate_amount:int = 0
var mutate_timer = 0
var beat = beats_to_bar
func _on_BeatTimer_timeout():
	beat += 1
	if(beat >= beats_to_bar):
		mutate_timer += 1
		if(mutate_timer > bars_to_mutate):
			$ChordPlayer.stop()
			$ChordPlayer.reset()
			$ChordPlayer.generate_chords(scale, num_chords)
			$ChordPlayer.generate_chords(scale, num_chords)
			$ArpPlayer.set_chords($ChordPlayer.chords)
			$ArpPlayer.stop()
			$ArpPlayer.reset()
			
			$MelodyPlayer.stop()
			$MelodyPlayer.reset()
			$MelodyPlayer.generate_melody(scale, $ChordPlayer.chords, num_melody)
			for _i in range(pattern_mutate_amount):
				var r = randi()%len(pattern)
				pattern[r] = not pattern[r]
			
			mutate_timer = 0
		if(chords):
			$ChordPlayer.play_next()
		beat = 0
	$MelodyPlayer.play_next(pattern[beat])
	if(drums):
		$DrumPlayer.beat()

func _on_ArpTimer_timeout():
	if(arp):
		$ArpPlayer.play_next()
