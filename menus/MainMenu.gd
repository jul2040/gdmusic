extends Control

var tracks = [
	{
		key=60, mode=0,
		bpm=120,
		melody=true,
		chords=true,
		arp=false,
		num_melody=16,
		chordiness=0.5,
		pattern=[
			true,
			true,
			false,
			true,
			true,
			false,
			true,
			true
		],
		pattern_mutate_amount=1,
		beats_to_bar=8, num_chords=4,
		chord_base_tones=2, chord_extra_tones=1,
		bars_to_mutate=8,
		chord_instrument=77, melody_instrument=10,
		chord_velocity=100,
		chord_note_offset=12,
		melody_velocity=150, melody_velocity_range=40,
		melody_min_note=70, melody_max_note=90
	},
	{
		key=60, mode=7,
		bpm=120,
		melody=true,
		chords=true,
		arp=true,
		num_melody=16,
		chordiness=0.75,
		interval_override=[
			5,1
		],
		pattern=[
			false,
			true,
			true,
			false,
			true,
			true
		],
		pattern_mutate_amount=0,
		beats_to_bar=6, num_chords=4,
		chord_base_tones=4, chord_extra_tones=1,
		bars_to_mutate=8,
		chord_instrument=70, arp_instrument=58, melody_instrument=74,
		arp_subdiv=1,
		arp_velocity=100, chord_velocity=100,
		arp_note_offset=-12, chord_note_offset=-12,
		melody_velocity=150, melody_velocity_range=40,
		melody_min_note=50, melody_max_note=70
	}
]

func _ready():
	_on_TrackSelector_value_changed($TrackSelector.value)
	_on_VolumeSlider_value_changed($VolumeSlider.value)

func _on_TrackSelector_value_changed(value):
	MusicBox.stop()
	MusicBox.load_song(tracks[value])
	MusicBox.play()

func _on_VolumeSlider_value_changed(value):
	MidiServer.set_volume(value)
